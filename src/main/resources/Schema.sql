
CREATE table hackathon.stocks(
       id int auto_increment,
       stockTicker varchar(40),
       price int(10),
       volume int,
       buyOrSell varchar(30),
       statusCode int,
       orderDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY (id)
       );