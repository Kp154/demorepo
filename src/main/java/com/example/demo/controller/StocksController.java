package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Stocks;
import com.example.demo.service.StocksService;

@RestController
@CrossOrigin("*")
@RequestMapping("api/stocks")
public class StocksController {

	@Autowired
	StocksService Stockservice;

	@GetMapping(value = "/")
	public List<Stocks> getAllStocks() {
		return Stockservice.getAllStocks();
	}
	@GetMapping(value = "/sortby{name}")
	public List<Stocks> getAllStocks(@PathVariable("name")String name) {
		//System.out.println(name);
		return Stockservice.sortBy(name);
	}
	
	@GetMapping(value = "/{id}")
	public Stocks getStocksById(@PathVariable("id") int id) {
	  return Stockservice.getStocks(id);
	}

	@PostMapping(value = "/")
	public Stocks addStocks(@RequestBody Stocks stocks) {
		return Stockservice.newStocks(stocks);
	}

	@PutMapping(value = "/")
	public Stocks editStocks(@RequestBody Stocks stocks) {
		return Stockservice.saveStocks(stocks);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStocks(@PathVariable int id) {
		return Stockservice.deleteStocks(id);
	}
	
}