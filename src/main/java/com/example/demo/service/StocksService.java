package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Stocks;
import com.example.demo.repository.StocksRepository;

@Service
public class StocksService {

	@Autowired
	private StocksRepository repository;

	public List<Stocks> getAllStocks() {
		return repository.getAllStocks();
	}
	
	public List<Stocks> sortBy(String name){
		return repository.sortBy(name);
	}
	
	public Stocks getStocks(int id) {
		return repository.getStocksById(id);
	}

	public Stocks saveStocks(Stocks stocks) {
		return repository.editStocks(stocks);
	}

	public Stocks newStocks(Stocks stocks) {
		return repository.addStocks(stocks);
	}

	public int deleteStocks(int id) {
		return repository.deleteStocks(id);
	}
}