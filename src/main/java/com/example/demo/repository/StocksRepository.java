package com.example.demo.repository;

import java.util.List;
import com.example.demo.entities.Stocks;

public interface StocksRepository {
	public List<Stocks> getAllStocks();

	public Stocks getStocksById(int id);

	public Stocks editStocks(Stocks stocks);

	public int deleteStocks(int id);

	public Stocks addStocks(Stocks stocks);
	
	public List<Stocks> sortBy(String name);
}
